package cake;

import java.util.Comparator;

public class CakeComparator implements Comparator<Cake> {

    private String orderType;

    CakeComparator(String orderType){

        this.orderType = orderType;
    }

    @Override
    public int compare(Cake firstCake, Cake secondCake) {
        if (orderType !=null){
        switch (orderType){
            case "title": return compareTitle(firstCake,secondCake);
            case "rate": return compareRate(firstCake,secondCake);
        }
        }
        return compareTitle(firstCake, secondCake);
    }

    private int compareRate(Cake firstCake, Cake secondCake) {
        int i =  Comparator.comparingInt(Cake::getRate).reversed().compare(firstCake,secondCake);
        if (i ==0)
            i = compareTitle(firstCake,secondCake);
        return i;
    }

    private int compareTitle(Cake c1, Cake c2){
        return c1.getTitle().compareToIgnoreCase(c2.getTitle());
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}
