package cake;

public class Cake {

    private String title;
    private String description;
    private int rate;
    private String imageSource;


    public Cake() {
        // JSON ?
    }

    public Cake(String title, String description, int rate, String imageSource) {

        this.title = title;
        this.description = description;
        this.rate = rate;
        this.imageSource = imageSource;
    }


    public String getImageSource() {
        return imageSource;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public int getRate() {
        return rate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String title) {
        this.description = title;
    }

    public void setRate(int title) {
        this.rate = title;
    }

    public void setImageSource(String title) {
        this.imageSource = title;
    }
}
