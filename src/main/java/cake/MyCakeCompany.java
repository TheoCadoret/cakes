package cake;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class MyCakeCompany {

    private static final Cake LEMON_CHEESECAKE = new Cake("Lemon cheesecake", "A cheese cake made of lemon", 8, "https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg");
    private static final Cake VICTORIA_SPONGE = new Cake("Victoria Sponge", "Sponge with jam", 3, "http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id-1001468_10.jpg");
    private static final Cake CARROT_CAKE = new Cake("Carrot Cake", "Bug bunny's favorite", 8, "http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg");
    private static final Cake BANANA_CAKE = new Cake("Banana cake", "Donkey kong's favorite", 6, "http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg");
    private static final Cake BIRTHDAY_CAKE = new Cake("Birthday Cake", "a yearly treat", 8, "http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg");

    private List<Cake> currentlyAvailableCakes;

    MyCakeCompany(){
        currentlyAvailableCakes = new ArrayList<>();
        setInitialCakes();
  }

    private void setInitialCakes() {
        currentlyAvailableCakes.clear();
        currentlyAvailableCakes.add(LEMON_CHEESECAKE);
        currentlyAvailableCakes.add(VICTORIA_SPONGE);
        currentlyAvailableCakes.add(CARROT_CAKE);
        currentlyAvailableCakes.add(BANANA_CAKE);
        currentlyAvailableCakes.add(BIRTHDAY_CAKE);
    }

    public List<Cake> getCurrentlyAvailableCakes(Comparator<Cake> comparator) {
        List<Cake> sortedList = currentlyAvailableCakes;
        sortedList.sort(comparator);
        return sortedList;
    }

    public Optional<Cake> getCake(String cakeName) {
        return currentlyAvailableCakes.stream().filter(cake -> cake.getTitle().equals(cakeName)).findFirst();
    }

    public void deleteCake(String cakeName) {
        currentlyAvailableCakes.removeIf(cake -> cake.getTitle().equals(cakeName));
    }

    public void initCakes() {
        setInitialCakes();
    }

    public void add(Cake cake) {
        currentlyAvailableCakes.add(cake);
    }
}
