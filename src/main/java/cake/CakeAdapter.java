package cake;


public class CakeAdapter {
    private final String title;
    private final String description;
    private final int rate;

    public CakeAdapter(Cake cake) {
        this(cake.getTitle(),cake.getDescription(),cake.getRate());
    }

    private CakeAdapter(String title, String description, int rate) {

        this.title = title;
        this.description = description;
        this.rate = rate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getRate() {
        return rate;
    }
}
