package cake;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class CakesController {

    private MyCakeCompany myCakeCompany;

    public CakesController() {
        myCakeCompany = new MyCakeCompany();
    }

    @RequestMapping(value = "/cakes")
    public List<CakeAdapter> getCakes(@RequestParam(value = "order", required = false) String order) {
        CakeComparator comparator = new CakeComparator(order);
        return myCakeCompany
                .getCurrentlyAvailableCakes(comparator)
                .stream()
                .map(CakeAdapter::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/cakes/{cakeName}", method = GET)
    public Cake getCake(@PathVariable String cakeName) {
        Optional<Cake> returnedCake = myCakeCompany.getCake(cakeName);
        return returnedCake.orElse(null);
    }

    @RequestMapping(value = "/cakes/{cakeName}", method = DELETE)
    public void deleteCake(@PathVariable String cakeName) {
        myCakeCompany.deleteCake(cakeName);
    }

    @RequestMapping(value = "/cakes/init", method = PUT)
    public void initCakeList() {
        myCakeCompany.initCakes();
    }

    @RequestMapping(value = "/cakes", method = POST)
    public void addCake(@RequestBody Cake cake) {
        myCakeCompany.add(cake);
    }

}